//
//  AppDelegate.swift
//  omehaz
//
//  Created by Yusuf Tezel on 18/01/2019.
//  Copyright © 2019 omehaz. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift

protocol NotifyAuthProcess: class {
    func loggedIn()
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var notifyAuthProcess: NotifyAuthProcess?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        let loginNavigationController = ClearNavigationController(rootViewController: TypeEmailViewController())
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.tintColor = Theme.tint
        window?.makeKeyAndVisible()
        window?.rootViewController = loginNavigationController
        return true
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return userActivity.webpageURL.flatMap(handlePasswordlessSignIn)!
    }
    
    func handlePasswordlessSignIn(withURL url: URL) -> Bool {
        guard notifyAuthProcess != nil else {
            return false
        }
        let link = url.absoluteString
        if Auth.auth().isSignIn(withEmailLink: link) {
            if let emailString = UserDefaults.standard.string(forKey: Library.email) {
                Auth.auth().signIn(withEmail: emailString, link: link) { (user, error) in
                    if let err = error {
                        print(err)
                    } else {
                        UserDefaults.standard.set(link, forKey: Library.link)
                        self.notifyAuthProcess?.loggedIn()
                    }
                }
                return true
            }
        }
        return false
    }
}
