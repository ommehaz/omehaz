//
//  WaitingForEmailComfirmViewController.swift
//  omehaz
//
//  Created by Yusuf Tezel on 23/01/2019.
//  Copyright © 2019 omehaz. All rights reserved.
//

import UIKit
import FirebaseAuth

class WaitingForEmailComfirmViewController: UIViewController, NotifyAuthProcess {
    
    func loggedIn() {
        infoLabel.isHidden = true
        logInConfirmedLabel.isHidden = false
    }
    
    var email: String!
    
    let spinner = UIActivityIndicatorView(style: .gray)
    
    let emailImage: UIImageView = {
        let image = UIImage(named: "email")
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    lazy var infoLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.text = "Der er nu sendt en bekræftelses email til \(email!), tryk på linket i emailen for at logge ind."
        titleLabel.font = UIFont(name: Theme.fontName, size: 20)
        titleLabel.numberOfLines = 0
        titleLabel.preferredMaxLayoutWidth = 300
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textAlignment = .center
        titleLabel.sizeToFit()
        return titleLabel
    }()
    
    let logInConfirmedLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.text = "Tilykke du er nu logget ind."
        titleLabel.font = UIFont(name: Theme.fontName, size: 20)
        titleLabel.numberOfLines = 0
        titleLabel.preferredMaxLayoutWidth = 300
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textAlignment = .center
        titleLabel.sizeToFit()
        return titleLabel
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Theme.background
        view.addSubview(spinner)
        view.addSubview(emailImage)
        view.addSubview(logInConfirmedLabel)
        view.addSubview(infoLabel)
        spinner.center = view.center
        spinner.startAnimating()
        
        emailImage.snp.makeConstraints {
            $0.width.equalTo(200)
            $0.height.equalTo(200)
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(infoLabel.snp.top).offset(-10)
        }
        
        infoLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(30)
            $0.trailing.equalToSuperview().offset(-30)
            $0.height.equalTo(200)
            $0.centerY.equalToSuperview().offset(40)
        }
        
        logInConfirmedLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(30)
            $0.trailing.equalToSuperview().offset(-30)
            $0.height.equalTo(200)
            $0.centerY.equalToSuperview().offset(40)
        }
        
        infoLabel.isHidden = true
        logInConfirmedLabel.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.notifyAuthProcess = self
        let settings = ActionCodeSettings.init()
        settings.url = URL(string: "https://omehaz.page.link/")
        settings.setIOSBundleID(Bundle.main.bundleIdentifier!)
        settings.handleCodeInApp = true
        settings.setAndroidPackageName("", installIfNotAvailable: false, minimumVersion: "12")
        Auth.auth().sendSignInLink(toEmail: email!, actionCodeSettings: settings, completion: { error in
            if let err = error {
                print(err)
            } else {
                UserDefaults.standard.set(self.email!, forKey: Library.email)
                self.spinner.stopAnimating()
                self.infoLabel.isHidden = false
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.notifyAuthProcess = nil
    }
}
