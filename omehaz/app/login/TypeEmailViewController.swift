//
//  ViewController.swift
//  omehaz
//
//  Created by Yusuf Tezel on 18/01/2019.
//  Copyright © 2019 omehaz. All rights reserved.
//

import UIKit
import SnapKit
import SkyFloatingLabelTextField
import SwiftIconFont
import IQKeyboardManagerSwift

class TypeEmailViewController: UIViewController {
    
    let image: UIImageView = {
        let image = UIImage(named: "shovel")
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    let emailTextField: SkyFloatingLabelTextFieldWithIcon = {
        let emailTextField = SkyFloatingLabelTextFieldWithIcon(frame: CGRect.zero)
        emailTextField.placeholder = "Indtast din email adresse her"
        emailTextField.title = "Email adresse"
        emailTextField.autocapitalizationType = .none
        emailTextField.errorColor = Theme.alternative!
        emailTextField.selectedTitleColor = .black
        emailTextField.autocorrectionType = .no
        emailTextField.contentVerticalAlignment = .center;
        emailTextField.textAlignment = .center
        emailTextField.keyboardType = UIKeyboardType.emailAddress
        emailTextField.textErrorColor = .black
        emailTextField.titleFormatter = { $0 }
        emailTextField.returnKeyType = .done
        return emailTextField
    }()
    
    let sloganLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.text = "Hvorfor vælge fra menuen, når du selv kan bygge."
        titleLabel.font = UIFont(name: Theme.fontName, size: 22)
        titleLabel.numberOfLines = 0
        titleLabel.preferredMaxLayoutWidth = 300
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textAlignment = .center
        titleLabel.sizeToFit()
        return titleLabel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Theme.background
        title = ""
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Send bekræftelse"
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        let informationBarImage = UIImage(from: .fontAwesome, code: "infocircle", textColor: .black, backgroundColor: .clear, size: CGSize(width: 30, height: 30))
        let informationBarButton = UIBarButtonItem(image: informationBarImage, style: .done, target: self, action: #selector(informationButtonClicked))
        navigationItem.rightBarButtonItems = [informationBarButton]
        
        emailTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        emailTextField.keyboardToolbar.addDoneOnKeyboardWithTarget(self, action: #selector(doneButtonClicked))
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        view.addSubview(emailTextField)
        view.addSubview(sloganLabel)
        view.addSubview(image)
        
        image.snp.makeConstraints {
            $0.height.equalTo(200)
            $0.width.equalTo(200)
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(sloganLabel.snp.top).offset(-10)
        }
        
        sloganLabel.snp.makeConstraints {
            $0.leading.equalToSuperview().offset(30)
            $0.trailing.equalToSuperview().offset(-30)
            $0.bottom.equalTo(emailTextField.snp.top).offset(-30)
        }
        
        emailTextField.snp.makeConstraints {
            $0.width.equalToSuperview().offset(-40)
            $0.height.equalTo(60)
            $0.centerY.equalToSuperview().offset(60)
            $0.centerX.equalToSuperview()
        }
    }
    
    @objc func textFieldDidChange(_ textfield: UITextField) {
        emailTextField.errorMessage = ""
    }
    
    @objc func informationButtonClicked() {
        resignFirstResponder()
        let alert = UIAlertController.init(title: "Info", message: "Du skal indtaste din email adresse og trykke på 'Send bekræftelse'. Der vil blive sendt en email til adressen med en bekræftelses link, som du skal aktivere ved at trykke på den. Linket skal aktiveres fra den samme telefon som appen køre i.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @objc func doneButtonClicked() {
        if let text = emailTextField.text {
            if(text.count < 3 || !text.contains("@")) {
                emailTextField.errorMessage = "Ugyldig email adresse"
            }
            else {
                resignFirstResponder()
                let waitingForEmailComfirmViewController = WaitingForEmailComfirmViewController()
                waitingForEmailComfirmViewController.email = text
                self.navigationController?.pushViewController(waitingForEmailComfirmViewController, animated: true)
            }
        }
    }
}

