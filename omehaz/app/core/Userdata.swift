//
//  Userdafualts.swift
//  omehaz
//
//  Created by Yusuf Tezel on 23/01/2019.
//  Copyright © 2019 omehaz. All rights reserved.
//

import UIKit

class Library {
    static let email = "email"
    static let link = "link"
}

class Theme {
    static let fontName = "Avenir-Light"
    static let background = UIColor.white
    static let tint = UIColor(named: "Tint")
    static let accent = UIColor(named: "Accent")
    static let alternative = UIColor(named: "Alternative")
    static let inverse = UIColor(named: "Inverse")
}
