//
//  ClearNavigationController.swift
//  omehaz
//
//  Created by Yusuf Tezel on 23/01/2019.
//  Copyright © 2019 omehaz. All rights reserved.
//

import UIKit

class ClearNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.backgroundColor = .clear
        self.navigationBar.isTranslucent = true
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
}
